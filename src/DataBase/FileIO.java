package DataBase;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**This class handles the loading of Properties files and CVS files, suing these to create Objects and Hashmaps appropriately
 * 
 * @author B[]Array & Paul Moggridge
 * @version 1.1 22/11/13
 */
public abstract class FileIO{
    /**This method loads a properties file and returns an object, instantiating the given constructor number in the given class.
     * Any Exceptions in this process will not crash the program but will print the stack trace of the exception responsible.
     * @param className This is the name of the class to be instantiated with the parameters at initFileName
     * @param initFileName A String containing properties whos values will be used in the given constructor
     * @return Object of type className that was instantiated using the given constructor using the paramters found in the
     * Properties file located at initFileName.
     */
    public static Object create(String className, String initFileName){
        Properties prop = new Properties();
        try{
            Class<?> c = getClass(className);
            prop.load(new FileInputStream(initFileName));
            int amount = 0;
            for(Enumeration e = prop.propertyNames(); e.hasMoreElements();){
                amount++;
                e.nextElement();
            }
            String[] params = new String[amount];
            for(int x=0; x<amount; x++)params[x] = prop.getProperty("arg[" + x + "]");
            return c.getDeclaredConstructor(String[].class).newInstance((Object)(params));
        }catch(Exception e){
            System.err.print(e);
            return null;
        }
    }
    
    /**This method loads in a CVS file from dataFileName, and appends a HashMap of data with Class of type className with the
     * parameters being passed into the given constructor number.
     * @param data A HashMap of unknown type to be appended with Objects created using the cvs file
     * @param className This is the name of the class to be instantiated with the parameters found in the cvs file
     * @param dataFileName The location of the cvs file to be used to instantiate objects of type className
     * @return An appended data HashMap with instantiated Objects created using the cvs file
     */
    @SuppressWarnings("unchecked") //Needed because the HashMap is of unknown type
    public static HashMap createDataBase(HashMap data, String className, String dataFileName){
        try{
            FileReader fileReader = new FileReader(dataFileName);
            BufferedReader reader = new BufferedReader(fileReader);
            Class<?> c = getClass(className);
            Constructor<?> ctor = c.getDeclaredConstructor(String[].class);
            String input = null;
            while((input = reader.readLine())!=null){
                String[] args = input.split(",");
                data.put(args[0], ctor.newInstance((Object)(args)));
            }
            reader.close();
            fileReader.close();
            return data;
        }catch(Exception e){
            System.err.print(e);
            return null;
        }
    }
    
    /**This method gets the position of an Object in a CVS database defined by the path in dataFileName by search sequentially for
     * the key in the CVS format file. This method assumes that the Object may be anywhere in the file and therefore searches all
     * locations from the start of the file for efficiency.
     * @param key is a String representing the unique identifier for the object in the database
     * @param dataFileName The location of the cvs file to be used to instantiate objects of type className
     * @return an Integer depicting the location of the Object in the file. A result of -1 is returned if the key can't be found.
     */
    public static int getDataBasePosition(String key, String dataFileName){
        try{
            FileReader fileReader = new FileReader(dataFileName);
            BufferedReader reader = new BufferedReader(fileReader);
            int position = 0;
            String input = null;
            while((input = reader.readLine())!=null){
                if(input.split(",")[0].equals(key))return position;
                position++;
            }
            reader.close();
            fileReader.close();
            return -1;
        }catch(Exception e){
            System.err.print(e);
            return -1;
        }
    }
    
    /**This method creates and returns an Object of Class type defined by className, where the position in the database
     * of the parameters to instantiate the Object are located by the position Integer. On failure or an Exception being
     * thrown, null is returned as the Object.
     * @param position an Integer containing the position of the Object to be loaded in the database
     * @param className This is the name of the class to be instantiated with the parameters found in the cvs file
     * @param dataFileName The location of the cvs file to be used to instantiate objects of type className
     * @return Object of type className that was instantiated using the given constructor using the paramters found in the
     * CVS file located at dataFileName. Null is returned if an Exception is thrown or the position was not reached.
     */
    public static Object getDataBaseObject(int position, String className, String dataFileName){
        try{
            FileReader fileReader = new FileReader(dataFileName);
            BufferedReader reader = new BufferedReader(fileReader);
            Class<?> c = getClass(className);
            int index = 0;
            String input = null;
            while((input = reader.readLine())!=null)if(index++==position)return c.getDeclaredConstructor(String[].class).newInstance((Object)(input.split(",")));
            return null;
        }catch(Exception e){
            System.err.print(e);
            return null;
        }
    }
    
    /**This method creates and returns an Object of Class type defined by className, where the position in the database
     * of the parameters to instantiate the Object are located by the key. On failure or an Exception being
     * thrown, null is returned as the Object. 
     * @param key A String representing a unique identifier which is searchable in the CVS file
     * @param className This is the name of the class to be instantiated with the parameters found in the cvs file
     * @param dataFileName The location of the cvs file to be used to instantiate objects of type className
     * @return Object that was retrieved from the CVS database and instantiated using className
     */
    public static Object getDataBaseObject(String key, String className, String dataFileName){ return getDataBaseObject(getDataBasePosition(key, dataFileName), className, dataFileName); }
    
    /**A method to remove an object which has a matching unique identifier to key from the CVS database
     * located at dataFileName.
     * @param key A String representing a unique identifier which is searchable in the CVS file
     * @param dataFileName The location of the cvs file where the Object reffered to by key is to be removed
     */
    public static boolean removeDataBaseObject(String key, String dataFileName){ return setDataBaseObject(key, null, dataFileName); }
    
    /**This method it responsible to setting a stored Object new values. To do this, the key is required
     * to identify the Object in the CVS file pointed to by dataFileName. The Object array params contains the
     * parameter information to be saved to file to represent the Object.
     * @param key A String representing a unique identifier which is searchable in the CVS file
     * @param params The parameters to be saved in a String or natural Object array. All of the types in this
     * array will have .toString() applied to them.
     * @param dataFileName The location of the cvs file to be used to instantiate objects of type className
     */
    public static boolean setDataBaseObject(String key, Object[] params, String dataFileName){
        int position = getDataBasePosition(key, dataFileName);
        if(position==-1)return false;
        String stringLine = objectsToString(params);
        try{
            File oldFile = new File(dataFileName);
            FileInputStream fileReader = new FileInputStream(oldFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileReader));
            File newFile = new File(dataFileName + "#temp"); //We can't write to a file while we read it!
            FileWriter writer = new FileWriter(newFile);
            int index = 0;
            String input = null;
            while((input = reader.readLine())!=null){
                if(index==position){
                    if(params!=null){
                        writer.write(stringLine);//Write line to replace
                        writer.write(System.getProperty("line.separator"));
                    }
                }else{
                    writer.write(input);//Write line that was there
                    writer.write(System.getProperty("line.separator"));
                }
                index++;
            }
            writer.flush();
            reader.close();
            fileReader.close();
            oldFile.delete(); //Delete Old File
            writer.close();
            newFile.renameTo(new File(dataFileName)); //Rename new File to name of Old File
            return true;
        }catch(Exception e){
            System.err.print(e);
            return false;
        }
    }
    
    /**This method adds a new Object to the CVS database by appending the dataFileName file with the new Object.
     * @param params The parameters to be saved in a String or natural Object array. All of the types in this
     * array will have .toString() applied to them.
     * @param dataFileName The location of the cvs file to be used to instantiate objects of type className
     */
    public static void addDataBaseObject(Object[] params, String dataFileName){
        String stringLine = objectsToString(params);
        try{
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(dataFileName, true)));
            out.println(stringLine);
            out.close();
        }catch(Exception e){ System.err.print(e); }
    }
    
    private static Class<?> getClass(String className){
        try{ return Class.forName(className); }
        catch(Exception e){
            System.err.print(e);
            return null;
        }
    }
    
    private static String objectsToString(Object[] objects){
        String stringLine = "";
        if(objects!=null){
            for(int x=0; x<objects.length; x++){
                stringLine += objects[x].toString();
                if(x<objects.length-1)stringLine += ",";
            }
            return stringLine;
        }else return null;
    }
}