package IO;

import java.util.Scanner;
import org.fusesource.jansi.AnsiConsole;

/**
 * This Class is responsible for simple input and output procedures inside the Terminal Window. There is some added value from normal input and output printing,
 * where colours are set and can be changed to represent errors. Not only this, there is a debug mode that can be set that only prints coloured debug messages
 * when it is set. Please note that by default this is off.
 * Known Operating Systems that the colours are known to work on are Unix and Windows based Systems. Users that are running systems that are not supported may
 * notice weird sybols on screen. This shouldn't stop the text from being readable.
 * @version 1.0
 * @author B[]Array
 */
public class TerminalIO implements IO{
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    
    private boolean debug = false;
    private String debugColour = ANSI_PURPLE;
    private String errColour = ANSI_RED;
    
    /**
     * This Constructor just sets the AnsiConsole to print correctly to the Operating System it is running on.
     */
    public TerminalIO(){ AnsiConsole.systemInstall(); }
    
    private Scanner scan = new Scanner(System.in);
    
    /**
     * This method is responsible for reading a String from the keyboard in the Terminal. Input is returned when the user presses enter.
     * @return A String containing the data read from the keyboard.
     */
    @Override
    public String read(){ return scan.nextLine(); }
    
    /**
     * This method is responsible for outputting a String to the relevant output the Class advertises. This should be as fast and as standard as possible.
     * @param msg The String to be output onto the Terminal Screen via a Standard Operating System supported method.
     */
    @Override
    public void write(String msg){ System.out.print(msg); }
    
    /**
     * This method is responsible for printing a coloured output to the terminal given a colour. Colours that are valid are available above as constants.
     * @param colour The colour to be printed to the terminal.
     * @param msg The String to be output onto the Terminal Screen in a given colour.
     */
    public void write(String colour, String msg){ AnsiConsole.out.print(colour + msg + ANSI_RESET); }
    
    /**
     * This method prints any errors that occur during the program. It prints these red by default onto the Terminal. You can use setErrColour to set the
     * colour that it prints to the terminal for errors. After it has finnished all printing, it terminates the whole program.
     * @param msg A String to be printed to the terminal that represents the error that occured.
     */
    @Override
    public void err(String msg){
        AnsiConsole.out.print("\n" + errColour + msg + ANSI_RESET + "\n");
        AnsiConsole.out.flush();
        System.exit(0);
    }
    
    /**
     * This method sets the colour of the error output. The error will only be printed once in the program so it is better to have the colour setup before
     * the occasion of running into an error.
     * @param colour The colour to set the error print message to.
     */
    public void setErrColour(String colour){ errColour = colour; }
    
    /**
     * This is how debug messages are dealt with. They are only printed if the debug option has been set to true. This means that the user can set the debug to
     * only switch on part way through the program and have the ability to turn it off reducing the amount of output the viewer sees. The colour for debug is a
     * purple colour by defualt.
     * @param msg A String containing the debug data to print to the screen in the set colour.
     */
    public void debug(String msg){
        if(debug){
            AnsiConsole.out.print("\n" + debugColour + msg + ANSI_RESET);
            AnsiConsole.out.flush();
        }
    }
    
    /**
     * This method sets the debug colour. The default debug colour is purple but is setable to any of the colours supported by the ANSI colour scheme. The primary
     * ones are constants in this class.
     * @param colour The colour to set the debug output messages as a String.
     */
    public void setDebugColour(String colour){ debugColour = colour; }
    
    /**
     * This method allows the user to set whether debug information is printed, where 'true' sets debug printing on and 'false' turns debug printing off.
     * @param debug Is a boolean value that allows for the debug messages to be printed or not printed.
     */
    public void setDebug(boolean debug){ this.debug = debug; }
    
    /**
     * This method allows for the status of the debug printing to be returned as a boolean so that it is possible to know whether debug information is being
     * printed to the console.
     * @return The boolean value that represents whether debug has been turned on or off.
     */
    public boolean getDebug(){ return debug; }
}