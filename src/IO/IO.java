package IO;

/**
 * The layout for all Classes that do IO. Something worth noting is that they will all be compatible with one another with no conversion needed. For example, it's
 * possible to run a file input straight into a network output. This gives extreme flexibility and allows programs to worry about other features while implementing
 * high levels of speed.
 * @version 1.0
 * @author B[]Array
 */
public interface IO{
    /**
     * This method is responsible for reading a String from the related input to the Class
     * @return A String containing the data related to the input
     */
    public String read();
    
    /**
     * This method is responsible for outputting a String to the relevant output the Class advertises. This should be as fast and as standard as possible.
     * @param msg The String to be output by the standard method offered by the Class.
     */
    public void write(String msg);
    
    /**
     * This method handles the eventuality of an error and will output in such a way that it is distinguishable from the standard output. AN example of this is the
     * Terminal class can output in a given colour so that the user can see it's an error.
     * @param msg A String message that represents the error in such a way that the user can understand it.
     */
    public void err(String msg);
}