package IO;

import java.io.InputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;

/**
 * 
 * @version 1.0
 * @author B[]Array
 */
public class ServerIO implements IO{ //TODO: Comment
    private Socket s;
    private ServerSocket ss;
    
    public ServerIO(int port) throws IOException{ ss = new ServerSocket(port); }
    
    @Override
    public String read(){
        try{ return read(accept()); }
        catch(IOException e){ throw new RuntimeException(e.getMessage()); }
    }
    
    public String read(Socket s) throws IOException{
        InputStream in = s.getInputStream();
        byte[] buf;
        String r = "";
        int rc = 1;
        while(rc>0){
            buf = new byte[4096];
            rc = in.read(buf);
            r = r.concat(new String(buf));
        }
        return r;
    }
    
    public Socket accept() throws IOException{ return ss.accept(); }
    
    @Override
    public void write(String msg){}
    
    @Override
    public void err(String msg){}
    
    public int getPort(){ return ss.getLocalPort(); }
}