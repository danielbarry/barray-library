package IO;

import java.io.IOException;
import java.net.Socket;

/**
 * 
 * @version 1.0
 * @author B[]Array
 */
public class ClientIO implements IO{ //TODO: Comment
    private Socket s;
    private String ip;
    private int port;
    
    public ClientIO(String ip, int port) throws IOException{ setSocket(new Socket(ip, port)); }
    
    private void setSocket(Socket s){ this.s = s; }
    
    @Override
    public String read(){ return null; }
    
    @Override
    public void write(String msg){
        try{
            s.getOutputStream().write(msg.getBytes());
            s.close();
        }catch(IOException e){ throw new RuntimeException(e.getMessage()); }
    }
    
    @Override
    public void err(String msg){}
}
