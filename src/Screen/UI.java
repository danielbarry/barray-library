package Screen;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import java.util.Scanner;

/**
 * This class makes it's self responsible for building a UI using the terminal for any Object thrown into it. It's important to note that is code is far from
 * complete, but should given a simple UI interface that allows you to use the public methods offered by the Class the object was created from. It will
 * also search for any data required which is not a primitive in an attempt to make sure that you are able to use UI. As it stands currently, you do need
 * to know how your methods work, as the primitives are not handled. There is simple error handling that prints an error message with the Exception thrown but again
 * this is not the best way to handle given errors.
 * 
 * I take no responsibility for this Class not working correctly, as it is inheritantly unstable and no such garuntee can be made. If any bugs are found
 * then please feel free to submit them.
 * 
 * @version 1.0 [05/12/2013]
 * @author B[]Array
 */
public class UI{
    private Class<?> clss;
    private Object object = null;
    private Method[] methods;
    private Class<?>[] returnTypes;
    
    private Scanner reader = new Scanner(System.in);
    private boolean running;
    
    /**
     * The Constructor is responsible for taking a class and the parameters to initialise it and create an Object, which the user can then perform a selection
     * of actions on it determined by the public methods available in the Class. The UI takes full control over the Thread it is run on when the start method
     * is called upon and will only terminate by selecting the releveant option in the menu.
     * @param clss A Class that is to be instantiated.
     * @param params The parameters to be used to instantiate the Class as an Object.
     */
    public UI(Class<?> clss, Object[] params){
        this.clss = clss;
        methods = clss.getDeclaredMethods();
        returnTypes = new Class<?>[methods.length];
        for(int x=0; x<returnTypes.length; x++)returnTypes[x] = methods[x].getReturnType();
        Class<?>[] paramTypes = new Class<?>[params.length];
        for(int x=0; x<params.length; x++)paramTypes[x] = params[x].getClass();
        try{ object = clss.getDeclaredConstructor(paramTypes).newInstance(params); }
        catch(Exception e){ System.err.println(e); }
        removeMethodsNotPublic();
    }
    
    private void removeMethodsNotPublic(){
        try{
            int count = 0;
            for(int x=0; x<methods.length; x++){
                if(Modifier.isPrivate(methods[x].getModifiers()))methods[x] = null;
                else count++;
            }
            Method[] temp = new Method[count];
            Class<?>[] returnTemp = new Class<?>[count];
            count = 0;
            for(int x=0; x<methods.length; x++){
                if(methods[x]!=null){
                    temp[count] = methods[x];
                    returnTemp[count++] = returnTypes[x];
                }
            }
            methods = temp;
            returnTypes = returnTemp;
        }catch(Exception e){ System.err.println(e); }
    }
    
    /**
     * This method must be run in order to start the Terminal menu. Once started, it assumes full control over the main thread and only returns when the relevant
     * menu option is selected, at which point it will return the Object it instantiated.
     * @return The Object which was instantiated by the Constructor with all actions performed on it thus far by the user.
     */
    public Object start(){
        running = true;
        while(running){
            try{ for(int x=0; x<methods.length; x++)println("[" + x + "] " + methods[x].getName()); }
            catch(Exception e){ System.err.println(e); }
            println("[" + methods.length + "] " + "quit");
            int response = userGetInt("\n>");
            if(response>=0&&response<=methods.length){
                if(response==methods.length)end();
                else{
                    Object o = handleMethod(response);
                    if(o!=null)println(o.toString());
                }
            }else println("Incorrect Response");
        }
        return object;
    }
    
    /**
     * A method that allows another thread to potentially stop the menu system. The current menu only checks whether it is still running once, leaving much to
     * be desired when wanting instantanious closure of the menu system. Potentially the user will be aloud to run one more method before the program closes if
     * called by another thread. Please note that this method is used internally and should not be overwritten until proper care has been taken as not to damage
     * current methods that rely on this method.
     */
    public void end(){ running = false; }
    
    private Object handleMethod(int methodNum){
        Method m = methods[methodNum];
        println(m.getName());
        try{
            Class<?>[] types = m.getParameterTypes();
            Object[] params = new Object[types.length];
            for(int x=0; x<types.length; x++){
                if(types[x].isPrimitive()){
                    params[x] = userGetString("\n" + types[x].getSimpleName()  + ">");
                    if(types[x].isAssignableFrom(boolean.class))params[x] = Boolean.parseBoolean((String)params[x]);
                    if(types[x].isAssignableFrom(byte.class))params[x] = Byte.parseByte((String)params[x]);
                    if(types[x].isAssignableFrom(char.class))params[x] = ((String)params[x]).toCharArray()[0];
                    if(types[x].isAssignableFrom(short.class))params[x] = Short.parseShort((String)params[x]);
                    if(types[x].isAssignableFrom(int.class))params[x] = Integer.parseInt((String)params[x]);
                    if(types[x].isAssignableFrom(long.class))params[x] = Long.parseLong((String)params[x]);
                    if(types[x].isAssignableFrom(float.class))params[x] = Float.parseFloat((String)params[x]);
                    if(types[x].isAssignableFrom(double.class))params[x] = Double.parseDouble((String)params[x]);
                }else params[x] = handleMethod(getAppropriateMethod(types[x]));
            }
            return m.invoke(object, params);
        }catch(Exception e){
            System.err.println(e);
            return null;
        }
    }
    
    private int getAppropriateMethod(Class<?> type){
        for(int x=0; x<returnTypes.length; x++)if(returnTypes[x].equals(type))return x;
        return -1;
    }
    
    private int userGetInt(String msg){
        try{ return Integer.parseInt(userGetString(msg));}
        catch(Exception e){ return -1; }
    }
    
    private String userGetString(String msg){
        print(msg);
        return reader.nextLine();
    }
    
    private void print(String msg){ System.out.print(msg); }
    
    private void println(String msg){ print("\n" + msg); }
}