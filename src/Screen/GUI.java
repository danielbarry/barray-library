package Screen;

import java.util.ArrayList;
import java.util.HashMap;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * This class makes it's self responsible for building a GUI using swing for any Object thrown into it. It's important to note that is code is far from
 * complete, but should given a simple GUI interface that allows you to use the public methods offered by the Class the object was created from. It will
 * also search for any data required which is not a primitive in an attempt to make sure that you are able to use GUI. As it stands currently, you do need
 * to know how your methods work, as the primitives are not handled. There is simple error handling that prints a dialog box with the Exception but again
 * this is not the best way to handle given errors.
 * 
 * I take no responsibility for this Class not working correctly, as it is inheritantly unstable and no such garuntee can be made. If any bugs are found
 * then please feel free to submit them.
 * 
 * @version 1.0 [05/12/2013]
 * @author B[]Array
 */
public class GUI extends JFrame implements ActionListener{
    private final int MAX_METHOD_FIND = 20;
    
    private Class<?> clss;
    private Object object = null;
    private Method[] methods;
    private Class<?>[] returnTypes;
    
    //Information for recursion Layers
    private String[] title = new String[MAX_METHOD_FIND];
    private JPanel[] current = new JPanel[MAX_METHOD_FIND];
    private Method[] currentMethod = new Method[MAX_METHOD_FIND];
    private Object[][] currentParams = new Object[MAX_METHOD_FIND][];
    private int layer = 0;
    private HashMap<JPanel, ArrayList<JTextField>> allText = new HashMap<JPanel, ArrayList<JTextField>>();
    
    /**
     * This constructor simply takes a title for the window to be created and the object to be used in the window and creates the window. As the swing
     * window is created in an update thread, this Contructor does pass control back to the position where this contrucor was initialised. The object
     * put into the contructor is not derefferenced at any point, meaning that all methods performed in the GUI are performed on the Object.
     * @param title A String containing the title to be used for the main window offering the user options/actions to perform on the Object.
     * @param object Is the Object that the GUI will use to give the user options/actions, where they are potentially able to adapt the Object using the
     * public methods that the Object makes available.
     */
    public GUI(String title, Object object){
        this.title[0] = title;
        this.object = object;
        clss = object.getClass();
        methods = clss.getDeclaredMethods();
        returnTypes = new Class<?>[methods.length];
        for(int x=0; x<returnTypes.length; x++)returnTypes[x] = methods[x].getReturnType();
        removeMethodsNotPublic();
        createMainWindow(title);
    }
    
    private void removeMethodsNotPublic(){
        try{
            int count = 0;
            for(int x=0; x<methods.length; x++){
                if(Modifier.isPrivate(methods[x].getModifiers()))methods[x] = null;
                else count++;
            }
            Method[] temp = new Method[count];
            Class<?>[] returnTemp = new Class<?>[count];
            count = 0;
            for(int x=0; x<methods.length; x++){
                if(methods[x]!=null){
                    temp[count] = methods[x];
                    returnTemp[count++] = returnTypes[x];
                }
            }
            methods = temp;
            returnTypes = returnTemp;
        }catch(Exception e){ displayError(e.toString()); }
    }
    
    private void createMainWindow(String title){
        current[layer] = createPanel(0, 1);
        setWindow(title);
        setSize(100, 300);
        for(int x=0; x<methods.length; x++)addButton(methods[x].getName());
        addButton("Exit");
        
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run(){
                setLocationRelativeTo(null);
                setDefaultCloseOperation(EXIT_ON_CLOSE);
                setVisible(true);
            }
        });
    }
    
    /**
     * Returns the Object that the operations are being performed on.
     * @return The Object that is being used by the GUI.
     */
    public Object getObject(){ return object; }
    
    private JPanel createPanel(int xLayout, int yLayout){
        JPanel pane = new JPanel();
        pane.setLayout(new GridLayout(xLayout, yLayout));
        allText.put(pane, new ArrayList<JTextField>());
        return pane;
    }
    
    private void setWindow(String title){
        getContentPane().removeAll();
        getContentPane().add(current[layer]);
        setTitle(title);
        validate();
        repaint();
    }
    
    private void addButton(String name){
        JButton b = new JButton(name);
        b.addActionListener(this);
        current[layer].add(b);
        validate();
        repaint();
    }
    
    private void addTextField(String name){
        JTextField t = new JTextField(name);
        current[layer].add(t);
        allText.get(current[layer]).add(t);
        validate();
        repaint();
    }
    
    /**
     * This is where the GUIBuilder handles the methods and decides how to preceed. Changing this method in any way could potentially change the way in
     * which the whole GUI works - so precaution should be used.
     * @param action Is an ActionEvent Object triggered by an action relating to the GUI. In this case the GUI only reacts to button related actions
     * (clicks).
     */
    @Override
    public void actionPerformed(ActionEvent event){
        Object obj = event.getSource();
        if(obj instanceof JButton){
            String buttonText = (JButton.class.cast(obj)).getText();
            if(layer==0){ //Main Window
                if(buttonText.equals("Exit"))System.exit(0);
                else{
                    layer++;
                    title[layer] = buttonText;
                    createMethodWindow(title[layer], stringToMethod(buttonText));
                }
            }else{ //Method Window
                if(buttonText.equals("Submit")){
                    if(layer==1){
                        displayOutput(getTitle(), methodReturn());
                        layer--;
                    }else{
                        Object o = methodReturn();
                        paramInsert(o, false);
                        layer--;
                    }
                }else layer = 0; //Cancel
                setWindow(title[layer]);
            }
        }
    }
    
    private Method stringToMethod(String name){
        for(int x=0; x<methods.length; x++)if(name.equals(methods[x].getName()))return methods[x];
        return null;
    }
    
    private Class<?>[] methodToTypes(Method method){ return method.getParameterTypes(); }
    
    private void createMethodWindow(String title, Method method){
        currentParams[layer] = new Object[methodToTypes(method).length];
        currentMethod[layer] = method;
        current[layer] = createPanel(0, 1);
        setWindow(title);
        for(Class<?> c : methodToTypes(method))if(c.isPrimitive()||c.isAssignableFrom(String.class))addTextField(c.getSimpleName());
        addButton("Submit");
        addButton("Cancel");
        handleNonParams(title);
    }
    
    private void handleNonParams(String title){
        Class<?>[] tempTypes = methodToTypes(currentMethod[layer]);
        for(int x=0; x<tempTypes.length; x++){
            Class<?> type = tempTypes[x];
            if(currentParams[layer][x]==null){
                if(!type.isPrimitive()&&!type.isAssignableFrom(String.class)){
                    layer++;
                    Method m = methods[getAppropriateMethod(type)];
                    this.title[layer] = m.getName();
                    createMethodWindow(this.title[layer], m);
                    return;
                }
            }
        }
    }
    
    private void paramInsert(Object obj, boolean searchForPrim){
        Class<?>[] tempTypes = methodToTypes(currentMethod[layer - 1]);
        for(int x=0; x<tempTypes.length; x++){
            Class<?> type = tempTypes[x];
            if(currentParams[layer - 1][x]==null)if(type.isPrimitive()==searchForPrim||type.isAssignableFrom(String.class))currentParams[layer - 1][x] = obj;
        }
    }
    
    private int getAppropriateMethod(Class<?> type){
        for(int x=0; x<returnTypes.length; x++)if(returnTypes[x].equals(type))return x;
        return -1;
    }
    
    private Object methodReturn(){
        if(layer>MAX_METHOD_FIND)displayError("Possible infinite recursion detected."); //TODO: Print methods involved in the attrocities
        ArrayList<JTextField> tAll = allText.get(current[layer]);
        String[] vals = new String[tAll.size()];
        for(int x=0; x<tAll.size(); x++)vals[x] = tAll.get(x).getText();
        Class<?>[] types = methodToTypes(currentMethod[layer]);
        Object[] params = currentParams[layer];
        int index = 0;
        for(int x=0; x<types.length; x++)if(types[x].isPrimitive()||types[x].isAssignableFrom(String.class))params[x] = castPrimitive(types[x], vals[index]);
        try{
            Object o = currentMethod[layer].invoke(object, params);
            if(layer>1)paramInsert(o, false);
            return o;
        }catch(Exception e){
            displayError(e.toString());
            return null;
        }
    }
    
    private Class<?> classToPrimitive(Class<?> val){
        if(val.isAssignableFrom(Boolean.class))val = boolean.class;
        if(val.isAssignableFrom(Byte.class))val = byte.class;
        if(val.isAssignableFrom(Character.class))val = char.class;
        if(val.isAssignableFrom(Short.class))val = short.class;
        if(val.isAssignableFrom(Integer.class))val = int.class;
        if(val.isAssignableFrom(Long.class))val = long.class;
        if(val.isAssignableFrom(Float.class))val = float.class;
        if(val.isAssignableFrom(Double.class))val = double.class;
        return val;
    }
    
    private Object castPrimitive(Class<?> type, Object val){
        try{
            if(type.isAssignableFrom(boolean.class))return Boolean.parseBoolean((String)val);
            if(type.isAssignableFrom(byte.class))return Byte.parseByte((String)val);
            if(type.isAssignableFrom(char.class))return ((String)val).toCharArray()[0];
            if(type.isAssignableFrom(short.class))return Short.parseShort((String)val);
            if(type.isAssignableFrom(int.class))return Integer.parseInt((String)val);
            if(type.isAssignableFrom(long.class))return Long.parseLong((String)val);
            if(type.isAssignableFrom(float.class))return Float.parseFloat((String)val);
            if(type.isAssignableFrom(double.class))return Double.parseDouble((String)val);
            if(type.isAssignableFrom(String.class))return (String)val;
        }catch(Exception e){ displayError(e.toString()); }
        return val;
    }
    
    /**
     * This method displays the output of the user completeing a method. The reason for this being public is that it may want to be overwritten in order
     * to output the users response in another way - for example to a file. In this case if the Object retured was null, nothing will be printed. If the
     * output is a valid Object, it will attempt to call the toString() method inherited by every Object. It simply prints the output of this request to
     * a textArea that adjusts it's scrollbars as they are needed.
     * @param title Is a String containing the title of the window to display output inside
     * @param output Is an Object that represents the output from the method called on the Object.
     */
    public void displayOutput(final String title, Object output){
        if(output==null)return;
        try{
            final Object printable = output.toString();
            SwingUtilities.invokeLater(new Runnable(){
                @Override
                public void run(){
                    JFrame out = new JFrame();
                    JPanel panel = createPanel(0, 1);
                    
                    out.getContentPane().add(panel);
                    out.setTitle(title);
                    out.setSize(300, 300);
                    
                    JTextArea t = new JTextArea((String)printable);
                    panel.add(t);
                    
                    JScrollPane s = new JScrollPane(t, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                    panel.add(s);
                    
                    out.setLocationRelativeTo(null);
                    out.setDefaultCloseOperation(HIDE_ON_CLOSE);
                    out.setVisible(true);
                }
            });
        }catch(Exception e){ displayError(e.toString()); }
    }
    
    private void displayError(String msg){ JOptionPane.showMessageDialog(new JFrame(), msg, "Error", JOptionPane.ERROR_MESSAGE); }
}